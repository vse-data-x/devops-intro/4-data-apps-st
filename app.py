import pandas as pd
import streamlit as st





st.title("Gitlab - Streamlit ML Application")
st.write("Deployed using Gitlab CI/CD  and Heroku")

words = ['university', 'college', 'school', 'army', 'tank', 'aircraft',
         'cat', 'dog', 'lion', 'puppy', 'student', 'teacher',
         'lesson', 'mobile', 'kitten']

corpus = ["prince", "princess", "nurse", "doctor", "banker", "man", "woman",
         "cousin", "neice", "king", "queen", "dude", "guy", "gal", "fire",
         "dog", "cat", "mouse", "red", "blue", "green", "yellow", "water",
         "person", "family", "brother", "sister"]

df = pd.DataFrame(corpus, columns=['words'])
st.dataframe(df)







x = st.slider("Slope", min_value = 0.01, max_value = 0.1, step=0.01)
y = st.slider("Noise", min_value=0.01, max_value=0.1, step=0.01)

st.write(f"x={x} y={y}")
